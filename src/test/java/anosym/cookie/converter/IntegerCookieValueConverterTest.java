package anosym.cookie.converter;

import org.junit.jupiter.api.Test;

import anosym.cookie.converter.IntegerCookieValueConverter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 9:38:51 PM
 */
public class IntegerCookieValueConverterTest {

  private final IntegerCookieValueConverter integerCookieValueConverter = new IntegerCookieValueConverter();

  @Test
  public void testCanConvertIntegers() {
    final boolean canConvert = integerCookieValueConverter.canConvert(Integer.class);
    assertThat(canConvert, is(true));
  }

  @Test
  public void testCannotConvertString() {
    final boolean canConvert = integerCookieValueConverter.canConvert(String.class);
    assertThat(canConvert, is(false));
  }

}
