package anosym.cookie;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * Just to ensure that there is at least one cookie readwrite controller service.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 31, 2016, 8:56:29 PM
 */
@ApplicationScoped
class DefaultCookieReadWriteControlService implements CookieReadWriteControlService {
}
