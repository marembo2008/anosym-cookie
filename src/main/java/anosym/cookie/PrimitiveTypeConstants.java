package anosym.cookie;

import com.google.common.collect.Maps;
import java.util.Map;
import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkArgument;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 26, 2015, 1:17:54 PM
 */
public final class PrimitiveTypeConstants {

    private static final Map<Class<?>, Class<?>> PRIMITIVE_TO_WRAPPER_MAPPER;

    static {
        PRIMITIVE_TO_WRAPPER_MAPPER = Maps.newHashMap();
        PRIMITIVE_TO_WRAPPER_MAPPER.put(char.class, Character.class);
        PRIMITIVE_TO_WRAPPER_MAPPER.put(byte.class, Byte.class);
        PRIMITIVE_TO_WRAPPER_MAPPER.put(short.class, Short.class);
        PRIMITIVE_TO_WRAPPER_MAPPER.put(int.class, Integer.class);
        PRIMITIVE_TO_WRAPPER_MAPPER.put(long.class, Long.class);
        PRIMITIVE_TO_WRAPPER_MAPPER.put(float.class, Float.class);
        PRIMITIVE_TO_WRAPPER_MAPPER.put(double.class, Double.class);
        PRIMITIVE_TO_WRAPPER_MAPPER.put(boolean.class, Boolean.class);
    }

    @Nonnull
    public static Class<?> getWrapperType(@Nonnull final Class<?> primitiveType) {
        checkArgument(primitiveType.isPrimitive(), "Provided class:{%s}, is not a primitive type", primitiveType);

        return PRIMITIVE_TO_WRAPPER_MAPPER.get(primitiveType);
    }
}
