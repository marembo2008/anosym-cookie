package anosym.cookie;

import javax.annotation.Nonnull;

import com.google.common.reflect.TypeToken;

/**
 * Provides pojos that must be annotated with {@link Cookie}
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 26, 2015, 11:39:24 AM
 */
public interface CookieProvider<T> {

    @Nonnull
    T provide();

    default Class<?> getSupportedType() {
        final TypeToken<T> typeToken = new TypeToken<T>(getClass()) {

            private static final long serialVersionUID = 309104475566522958L;

        };

        return typeToken.getRawType();
    }

}
