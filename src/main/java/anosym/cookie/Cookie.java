package anosym.cookie;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.enterprise.util.Nonbinding;

/**
 * A cookie may not contain another cookie as a member, it will not be evaluated, unless a converter is provided for it.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 8:58:24 PM
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Cookie {

  /**
   * The name of the cookie, if not specified, the simplename of the cookie class is used.
   */
  @Nonbinding
  String value() default "";

  @Nonbinding
  int maxAge() default Integer.MAX_VALUE;

  @Nonbinding
  String path() default "/";

  /**
   * If not specified, defaults to current domain.
   */
  @Nonbinding
  String domain() default "";

  @Nonbinding
  boolean httpOnly() default false;

  /**
   * Only considered if {@link #secureOption()} == {@link SecureOption#USE_SPECIFIED_SECURITY}.
   */
  @Nonbinding
  boolean secure() default false;

  @Nonbinding
  SecureOption secureOption() default SecureOption.USE_SPECIFIED_SECURITY;

  enum SecureOption {

    USE_REQUEST_SECURITY,
    USE_SPECIFIED_SECURITY;

  }

}
