package anosym.cookie;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Any field marked by this annotation is considered a cookie value, all the rest are ignored.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 10:49:48 PM
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CookieProperty {

    //Ensure unique indices
    int index() default 0;
}
