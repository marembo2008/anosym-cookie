package anosym.cookie;

import javax.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 21, 2016, 10:40:43 AM
 */
public interface CookieReadWriteControlService {

    default boolean readCookie(@Nonnull final String cookieName) {
        return true;
    }

    default boolean writeCookie(@Nonnull final String cookieName) {
        return true;
    }
}
