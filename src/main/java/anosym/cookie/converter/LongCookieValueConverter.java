package anosym.cookie.converter;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 9:37:22 PM
 */
@ApplicationScoped
public class LongCookieValueConverter implements CookieValueConverter<Long> {

  @Nonnull
  @Override
  public String convertFromValue(@Nonnull final Long value) {
    checkNotNull(value, "The value must not be null");

    return String.valueOf(value);
  }

  @Nonnull
  @Override
  public Long convertToValue(@Nonnull final String stringValue) {
    checkNotNull(stringValue, "The stringValue must not be null");

    return Long.parseLong(stringValue);
  }

}
