package anosym.cookie.converter;

import com.google.common.reflect.TypeToken;
import javax.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 9:33:20 PM
 */
public interface CookieValueConverter<T> {

    @Nonnull
    String convertFromValue(@Nonnull final T value);

    @Nonnull
    T convertToValue(@Nonnull final String stringValue);

    default boolean canConvert(@Nonnull final Class<?> valueType) {
        final Class<?> type = getSupportedType();
        return type == valueType;
    }

    default Class<?> getSupportedType() {
        final TypeToken<T> typeToken = new TypeToken<T>(getClass()) {
        };

        return typeToken.getRawType();
    }
}
