package anosym.cookie.converter;

import java.math.BigDecimal;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 9:49:04 PM
 */
@ApplicationScoped
public class BigDecimalCookieValueConverter implements CookieValueConverter<BigDecimal> {

  @Nonnull
  @Override
  public String convertFromValue(@Nonnull final BigDecimal value) {
    checkNotNull(value, "The value must not be null");

    return value.toString();
  }

  @Nonnull
  @Override
  public BigDecimal convertToValue(@Nonnull final String stringValue) {
    checkNotNull(stringValue, "The stringValue must not be null");

    return new BigDecimal(stringValue);
  }

}
