package anosym.cookie.converter;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 9:44:08 PM
 */
@ApplicationScoped
public class StringCookieValueConverter implements CookieValueConverter<String> {

  @Nonnull
  @Override
  public String convertFromValue(@Nonnull final String value) {
    return checkNotNull(value, "The value must not be null");
  }

  @Nonnull
  @Override
  public String convertToValue(@Nonnull final String stringValue) {
    return checkNotNull(stringValue, "The stringValue must not be null");
  }

}
