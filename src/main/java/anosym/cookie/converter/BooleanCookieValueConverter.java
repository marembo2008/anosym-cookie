package anosym.cookie.converter;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 26, 2015, 1:14:56 PM
 */
@ApplicationScoped
public class BooleanCookieValueConverter implements CookieValueConverter<Boolean> {

  @Override
  public String convertFromValue(@Nonnull final Boolean value) {
    return value ? "1" : "0";
  }

  @Override
  public Boolean convertToValue(@Nonnull final String stringValue) {
    checkNotNull(stringValue, "The stringValue must not be null");

    switch (stringValue) {
      case "1":
        return true;
      default:
        return false;
    }
  }

}
