package anosym.cookie.converter;

import java.util.Calendar;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 9:46:24 PM
 */
@ApplicationScoped
public class CalendarCookieValueConverter implements CookieValueConverter<Calendar> {

  @Nonnull
  @Override
  public String convertFromValue(@Nonnull final Calendar value) {
    checkNotNull(value, "The value must not be null");

    return String.valueOf(value.getTimeInMillis());
  }

  @Nonnull
  @Override
  public Calendar convertToValue(@Nonnull final String stringValue) {
    checkNotNull(stringValue, "The stringValue must not be null");

    final long millis = Long.parseLong(stringValue);
    final Calendar now = Calendar.getInstance();
    now.setTimeInMillis(millis);
    return now;
  }

}
