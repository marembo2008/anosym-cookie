package anosym.cookie;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

import anosym.cookie.converter.CookieValueConverter;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static java.lang.String.format;
import static anosym.cookie.PrimitiveTypeConstants.getWrapperType;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2015, 9:00:47 PM
 */
@ApplicationScoped
public class CookieService implements Filter {

  private static final Ordering<Field> COOKIE_FIELD_ORDER = Ordering.from(
          (field1, field2) -> {
            final CookieProperty prop1 = field1.getAnnotation(CookieProperty.class);
            final int index1 = prop1.index();

            //field2
            final CookieProperty prop2 = field2.getAnnotation(CookieProperty.class);
            final int index2 = prop2.index();

            //index comparison:
            final int cmp = Integer.valueOf(index1).compareTo(index2);
            return cmp != 0 ? cmp : field1.getName().compareToIgnoreCase(field2.getName());
          });

  private static final List<String> UNSUPPORTED_PATHS = ImmutableList.of(
          "/security/captcha.*",
          "/javax\\.faces\\.resource.*",
          "/resources.*",
          "/fonts.*",
          //If we want to update the list of restricted countries, for example, and we are restricted initially :-(
          ".*/ConfigurationChangeListenerService.*"
  );

  private static final String EXCLUDED_RESOURCES_PARAMETER_NAME = "cookie.paths.excluded";

  private static final Splitter EXCLUDED_RESOURCES_PARAMETER_SPLITTER
          = Splitter.on(CharMatcher.whitespace()).trimResults().omitEmptyStrings();

  private static final String COOKIE_FORMAT = "(%s::%s)";

  private static final CharMatcher TRIMMING_MATCHER = CharMatcher.anyOf(")(").or(CharMatcher.whitespace());

  private static final Splitter COOKIE_VALUE_SPLITTER
          = Splitter.on(")(").trimResults(TRIMMING_MATCHER).omitEmptyStrings();

  @Any
  @Inject
  private Instance<CookieProvider<?>> cookieProviders;

  @Inject
  private Instance<CookieReadWriteControlService> cookieReadWriteControlService;

  private Map<Class<?>, CookieValueConverter<?>> cookieValueConverters;

  private List<String> excludedPathRegex = ImmutableList.of();

  @PostConstruct
  void makeSureProvidedCookiesAreAnnotated() {
    cookieProviders.forEach((cookieprovider) -> {
      final Class<?> cookieClass = cookieprovider.getSupportedType();
      checkState(cookieClass.isAnnotationPresent(Cookie.class), "A Cookie object must be annotated with @Cookie");
    });

  }

  @Inject
  void injectCookieValueConverter(@Any @Nonnull final Instance<CookieValueConverter<?>> cookieValueConverters) {
    this.cookieValueConverters = ImmutableList
            .copyOf(cookieValueConverters)
            .stream()
            .collect(Collectors.toMap((cvc) -> cvc.getSupportedType(), Function.identity()));
  }

  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
    final HttpServletRequest servletRequest = (HttpServletRequest) request;
    final CookieHttpServletResponse servletResponse = new CookieHttpServletResponse((HttpServletResponse) response);
    final String path = servletRequest.getServletPath();
    final boolean included = !isUnsupportedRequestPath(path) && excludedPathRegex
            .stream()
            .noneMatch((regex) -> Pattern.matches(regex, path));

    if (included) {
      loadCookies(servletRequest, servletResponse);
      servletResponse.addPreprocessor(() -> writeCookies(servletRequest, servletResponse));
    }

    chain.doFilter(servletRequest, servletResponse);
  }

  @Override
  public void destroy() {
    //Do Nothing
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    final String excludedPaths = filterConfig.getInitParameter(EXCLUDED_RESOURCES_PARAMETER_NAME);
    if (Objects.isNull(excludedPaths)) {
      return;
    }

    this.excludedPathRegex = EXCLUDED_RESOURCES_PARAMETER_SPLITTER.splitToList(excludedPaths);
  }

  @VisibleForTesting
  boolean isUnsupportedRequestPath(@Nonnull final String requestPath) {
    return UNSUPPORTED_PATHS
            .stream()
            .anyMatch((unsupportedPathRegex) -> Pattern.matches(unsupportedPathRegex, requestPath));
  }

  @Nullable
  private CookieProvider getCookieProvider(@Nonnull
          final String cookieName
  ) {
    return ImmutableList.copyOf(cookieProviders)
            .stream()
            .filter((cookieProvider) -> {
              final Class<?> cookieClass = cookieProvider.getSupportedType();
              final Cookie cookieInfo = cookieClass.getAnnotation(Cookie.class);
              final String derivedCookieName
                      = firstNonNull(emptyToNull(cookieInfo.value()), cookieClass.getSimpleName());

              return Objects.equals(derivedCookieName, cookieName);
            })
            .findFirst()
            .orElse(null);
  }

  private void loadCookies(@Nonnull final HttpServletRequest servletRequest,
                           @Nonnull final HttpServletResponse servletResponse) {
    final jakarta.servlet.http.Cookie[] httpCookies = servletRequest.getCookies();
    if (httpCookies == null) {
      return;
    }

    for (final jakarta.servlet.http.Cookie httpCookie : httpCookies) {
      final String cookieName = httpCookie.getName();
      if (servletRequest.getAttribute(cookieName) != null) {
        continue;
      }

      if (!readCookie(cookieName)) {
        continue;
      }

      final CookieProvider cookieProvider = getCookieProvider(cookieName);
      if (cookieProvider == null) {
        continue;
      }

      final Object cookie = cookieProvider.provide();
      checkNotNull(cookie,
                   "The following provider provided null cookie object: {%s}",
                   cookieProvider.getClass());

      final List<Field> orderedFields = ordereredFields(cookie);
      final String cookieValue = new String(Base64.getDecoder().decode(httpCookie.getValue()));
      final List<String> fieldValues = COOKIE_VALUE_SPLITTER.splitToList(cookieValue);
      for (final String rawValue : fieldValues) {
        final String[] posValue = rawValue.split("::");
        final int cookieValuePosition = Integer.parseInt(posValue[0]);
        final Field field = orderedFields.get(cookieValuePosition);
        field.setAccessible(true);

        final String value = posValue.length > 1 ? posValue[1] : "";
        final Class<?> fieldType = field.getType();
        final Class<?> valueType = fieldType.isPrimitive() ? getWrapperType(fieldType) : fieldType;
        final CookieValueConverter cookieValueConverter = cookieValueConverters.get(valueType);
        checkState(cookieValueConverter != null,
                   "There is no converter registered for cookie value type: {%s}",
                   valueType);

        final Object cookieFieldValue = cookieValueConverter.convertToValue(value);
        try {
          field.set(cookie, cookieFieldValue);
        } catch (final IllegalArgumentException | IllegalAccessException ex) {
          throw Throwables.propagate(ex);
        }
      }

      //This method can be called several times during redirects, and at that time, probably new cookie values may be overriden.
      servletRequest.setAttribute(cookieName, cookie);
    }
  }

  @VisibleForTesting
  void writeCookies(@Nonnull final HttpServletRequest servletRequest,
                    @Nonnull final HttpServletResponse servletResponse) {
    cookieProviders
            .forEach((cookieProvider) -> {
              final Object cookie = cookieProvider.provide();
              checkNotNull(cookie,
                           "The following provider provided null cookie object: {%s}",
                           cookieProvider.getClass());

              final Class<?> cookieClass = cookie.getClass();
              final String cookieName = getCookieName(cookieClass);

              if (!writeCookie(cookieName)) {
                return;
              }

              final Cookie cookieInfo = cookieClass.getAnnotation(Cookie.class);
              final StringBuilder cookieValue = new StringBuilder();
              final List<Field> orderedFields = ordereredFields(cookie);
              for (int i = 0; i < orderedFields.size(); i++) {
                final Field fieldOption = orderedFields.get(i);
                try {
                  final Class<?> fieldType = fieldOption.getType();
                  final Class<?> valueType = fieldType.isPrimitive() ? getWrapperType(fieldType) : fieldType;
                  final CookieValueConverter cookieValueConverter = cookieValueConverters.get(valueType);
                  checkState(cookieValueConverter != null,
                             "There is no converter registered for cookie value type: {%s}",
                             valueType);

                  fieldOption.setAccessible(true);
                  final Object value = fieldOption.get(cookie);
                  if (value == null) {
                    continue;
                  }

                  final String convertedValue = cookieValueConverter.convertFromValue(value);
                  if (isNullOrEmpty(convertedValue)) {
                    continue;
                  }

                  cookieValue.append(format(COOKIE_FORMAT, i, convertedValue));
                } catch (final IllegalArgumentException | IllegalAccessException ex) {
                  throw Throwables.propagate(ex);
                }
              }

              final String value = Base64.getEncoder().encodeToString(cookieValue.toString().trim().getBytes());
              jakarta.servlet.http.Cookie httpCookie
                      = createCookie(cookieName, value, cookieInfo, servletRequest);
              if (value.isEmpty()) {
                //Deleting the cookie, if it exists
                httpCookie.setMaxAge(0);
              }

              servletResponse.addCookie(httpCookie);
            });
  }

  @Nonnull
  private String getCookieName(@Nonnull final Class<?> cookieClass) {
    final Cookie cookieInfo = cookieClass.getAnnotation(Cookie.class);
    return firstNonNull(emptyToNull(cookieInfo.value()), cookieClass.getSimpleName());
  }

  @VisibleForTesting
  jakarta.servlet.http.Cookie createCookie(@Nonnull final String cookieName,
                                           @Nonnull final String cookieValue,
                                           @Nonnull final Cookie cookieInfo,
                                           @Nonnull final HttpServletRequest servletRequest) {
    final jakarta.servlet.http.Cookie cookie
            = new jakarta.servlet.http.Cookie(cookieName, cookieValue);
    cookie.setMaxAge(cookieInfo.maxAge());
    cookie.setPath(cookieInfo.path());
    if (!isNullOrEmpty(cookieInfo.domain())) {
      cookie.setDomain(cookieInfo.domain());
    }

    cookie.setHttpOnly(cookieInfo.httpOnly());

    final boolean isSecure = cookieInfo.secureOption() == Cookie.SecureOption.USE_SPECIFIED_SECURITY
            ? cookieInfo.secure() : servletRequest.isSecure();
    cookie.setSecure(isSecure);

    return cookie;
  }

  private List<Field> ordereredFields(@Nonnull final Object cookie) {
    final List<Field> fields = Lists.newArrayList();
    findAllCookieFields(cookie.getClass(), fields);

    return COOKIE_FIELD_ORDER.immutableSortedCopy(fields);
  }

  private void findAllCookieFields(@Nonnull final Class<?> clazz, @Nonnull final List<Field> cookieValueFields) {
    if (clazz == Object.class) {
      return;
    }

    for (final Field field : clazz.getDeclaredFields()) {
      if (field.isAnnotationPresent(CookieProperty.class)) {
        cookieValueFields.add(field);
      }
    }

    findAllCookieFields(clazz.getSuperclass(), cookieValueFields);
  }

  private boolean readCookie(@Nonnull final String cookieName) {
    return ImmutableList
            .copyOf(cookieReadWriteControlService)
            .stream()
            .allMatch((readWriteControllService) -> readWriteControllService.readCookie(cookieName));
  }

  private boolean writeCookie(@Nonnull final String cookieName) {
    return ImmutableList
            .copyOf(cookieReadWriteControlService)
            .stream()
            .allMatch((readWriteControllService) -> readWriteControllService.writeCookie(cookieName));
  }

}
