package anosym.cookie;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Nonnull;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 21, 2018, 11:46:42 PM
 */
public class CookieHttpServletResponse extends HttpServletResponseWrapper {

  private final List<Runnable> processorsBeforeOutputIsWritten;

  private final AtomicBoolean preprocessorsRan;

  public CookieHttpServletResponse(final HttpServletResponse response) {
    super(response);
    processorsBeforeOutputIsWritten = new ArrayList<>();
    preprocessorsRan = new AtomicBoolean(false);
  }

  public void addPreprocessor(@Nonnull final Runnable runnable) {
    processorsBeforeOutputIsWritten.add(runnable);
  }

  @Override
  public ServletOutputStream getOutputStream() throws IOException {
    if (preprocessorsRan.compareAndSet(false, true)) {
      processorsBeforeOutputIsWritten.forEach(Runnable::run);
    }

    return super.getOutputStream();
  }

  @Override
  public PrintWriter getWriter() throws IOException {
    if (preprocessorsRan.compareAndSet(false, true)) {
      processorsBeforeOutputIsWritten.forEach(Runnable::run);
    }

    return super.getWriter();
  }

}
